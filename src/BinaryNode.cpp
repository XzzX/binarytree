#include "BinaryNode.h"

BinaryNode::BinaryNode(BinaryNode* parent, std::unique_ptr<TreeData>&& data)
    : parent_(parent)
    , data_(std::move(data))
{

}

BinaryNode::~BinaryNode()
{

}
