#pragma once

#include <memory>

using TreeData = int;

class BinaryNode
{
public:
    BinaryNode(BinaryNode* parent, std::unique_ptr<TreeData>&& data);
    ~BinaryNode();

    int                         balance     = 0;
    BinaryNode*                 parent_     = nullptr;
    std::unique_ptr<BinaryNode> leftChild_  = nullptr;
    std::unique_ptr<BinaryNode> rightChild_ = nullptr;

    std::unique_ptr<TreeData>   data_       = nullptr;
};
