#include "BinaryTree.h"

#include <cassert>
#include <fstream>
#include <queue>

BinaryTree::iterator& BinaryTree::iterator::operator++()
{
    auto current = node_;

    if (current->rightChild_)
    {
        current = current->rightChild_.get();
        while (current->leftChild_)
        {
            current = current->leftChild_.get();
        }
    } else
    {
        if (current->parent_)
        {
            if (current == current->parent_->leftChild_.get())
            {
                //left child
                current = current->parent_;
            } else
            {
                //right child
                while (current == current->parent_->rightChild_.get())
                {
                    current = current->parent_;
                    if (!current->parent_)
                    {
                        node_ = nullptr;
                        return *this;
                    }
                }
                current = current->parent_;
            }
        } else
        {
            node_ = nullptr;
            return *this;
        }
    }

    node_ = current;
    return *this;
}

BinaryTree::BinaryTree()
{

}

BinaryTree::iterator BinaryTree::begin() const
{
    if (!root_) return iterator();
    auto current = root_.get();
    while (current->leftChild_)
    {
        current = current->leftChild_.get();
    }
    return iterator(current);
}
BinaryTree::iterator BinaryTree::end() const
{
    return iterator();
}

std::unique_ptr<BinaryNode>& BinaryTree::rotateLeft( std::unique_ptr<BinaryNode>& parent, std::unique_ptr<BinaryNode>& rightChild )
{
    //break connection to parent
    std::unique_ptr<BinaryNode> subtree = std::move(rightChild);
    //give parent my left child
    rightChild = std::move(subtree->leftChild_);
    if (rightChild) rightChild->parent_ = parent.get();
    auto pParent = parent->parent_;
    subtree->leftChild_ = std::move(parent);
    subtree->leftChild_->parent_ = subtree.get();
    parent = std::move(subtree);
    parent->parent_ = pParent;

    parent->balance = 0;
    parent->leftChild_->balance = 0;

    return parent;
}

std::unique_ptr<BinaryNode>& BinaryTree::rotateRight( std::unique_ptr<BinaryNode>& parent, std::unique_ptr<BinaryNode>& leftChild )
{
    //break connection to parent
    std::unique_ptr<BinaryNode> subtree = std::move(leftChild);
    //give parent my right child
    leftChild = std::move(subtree->rightChild_);
    if (leftChild) leftChild->parent_ = parent.get();
    auto pParent = parent->parent_;
    subtree->rightChild_ = std::move(parent);
    subtree->rightChild_->parent_ = subtree.get();
    parent = std::move(subtree);
    parent->parent_ = pParent;

    parent->balance = 0;
    parent->rightChild_->balance = 0;

    return parent;
}

std::unique_ptr<BinaryNode>& BinaryTree::rotateRightLeft( std::unique_ptr<BinaryNode>& parent, std::unique_ptr<BinaryNode>& rightChild )
{
    std::unique_ptr<BinaryNode> subtree = std::move(rightChild->leftChild_);
    rightChild->leftChild_ = std::move(subtree->rightChild_);
    if (rightChild->leftChild_) rightChild->leftChild_->parent_ = rightChild.get();
    subtree->rightChild_ = std::move(rightChild);
    subtree->rightChild_->parent_ = subtree.get();
    rightChild = std::move(subtree);
    rightChild->parent_ = parent.get();

    subtree = std::move(rightChild);
    rightChild = std::move(subtree->leftChild_);
    if (rightChild) rightChild->parent_ = parent.get();
    auto pParent = parent->parent_;
    subtree->leftChild_ = std::move(parent);
    subtree->leftChild_->parent_ = subtree.get();
    parent = std::move(subtree);
    parent->parent_ = pParent;

    if (parent->balance > 0)
    {
        parent->leftChild_->balance  = -1;
        parent->rightChild_->balance =  0;
    } else if (parent->balance == 0)
    {
        parent->leftChild_->balance  =  0;
        parent->rightChild_->balance =  0;
    } else if (parent->balance < 0)
    {
        parent->leftChild_->balance  =  0;
        parent->rightChild_->balance =  1;
    }
    parent->balance = 0;

    return parent;
}

std::unique_ptr<BinaryNode>& BinaryTree::rotateLeftRight( std::unique_ptr<BinaryNode>& parent, std::unique_ptr<BinaryNode>& leftChild )
{
    std::unique_ptr<BinaryNode> subtree = std::move(leftChild->rightChild_);
    leftChild->rightChild_ = std::move(subtree->leftChild_);
    if (leftChild->rightChild_) leftChild->rightChild_->parent_ = leftChild.get();
    subtree->leftChild_ = std::move(leftChild);
    subtree->leftChild_->parent_ = subtree.get();
    leftChild = std::move(subtree);
    leftChild->parent_ = parent.get();

    subtree = std::move(leftChild);
    leftChild = std::move(subtree->rightChild_);
    if (leftChild) leftChild->parent_ = parent.get();
    auto pParent = parent->parent_;
    subtree->rightChild_ = std::move(parent);
    subtree->rightChild_->parent_ = subtree.get();
    parent = std::move(subtree);
    parent->parent_ = pParent;

    if (parent->balance > 0)
    {
        parent->leftChild_->balance  =  0;
        parent->rightChild_->balance = -1;
    } else if (parent->balance == 0)
    {
        parent->leftChild_->balance  =  0;
        parent->rightChild_->balance =  0;
    } else if (parent->balance < 0)
    {
        parent->leftChild_->balance  =  1;
        parent->rightChild_->balance =  0;
    }
    parent->balance = 0;

    return parent;
}

BinaryTree::iterator BinaryTree::insert( std::unique_ptr<TreeData>&& data )
{
    auto current = plainInsert( std::move(data) );
    if (!current->parent_) return iterator(current); // root element was inserted -> return
    auto parent  = current->parent_;

    // update balance of parent
    if (current == parent->rightChild_.get())
    {
        parent->balance += 1;
        assert( parent->balance == 0 || parent->balance == 1 );
    } else
    {
        parent->balance -= 1;
        assert( parent->balance == -1 || parent->balance == 0 );
    }
    if (parent->balance == 0) return iterator(current); // depth was not changed -> return

    current = parent; // height of subtree was increased
    parent  = current->parent_;
    while (parent)
    {
        if (current == parent->rightChild_.get())
        {
            if (parent->balance > 0)
            {
                // I am the right child and parent's balance was already right heavy
                if (current->balance < 0)
                {
                    parent = rotateRightLeft(getUniquePtr(parent), parent->rightChild_).get();
                } else if (current->balance == 0)
                {
                    //difference was absorbed
                    break;
                } else
                {
                    parent = rotateLeft(getUniquePtr(parent), parent->rightChild_).get();
                }
            } else
            {
                if (parent->balance < 0)
                {
                    assert(parent->balance == -1); //always true?
                    //difference is absorbed here
                    parent->balance = 0;
                    break;
                }
                assert(parent->balance == 0);
                if (current->balance == 0)
                {
                    parent->balance = 0;
                } else
                {
                    parent->balance = 1;
                }
            }
        } else
        {
            assert(current == parent->leftChild_.get());
            if (parent->balance < 0)
            {
                // I am the right child and parent's balance was already right heavy
                if (current->balance > 0)
                {
                    parent = rotateLeftRight(getUniquePtr(parent), parent->leftChild_).get();
                } else if (current->balance == 0)
                {
                    //difference was absorbed
                    break;
                } else
                {
                    parent = rotateRight(getUniquePtr(parent), parent->leftChild_).get();
                }

            } else
            {
                if (parent->balance > 0)
                {
                    assert(parent->balance == 1); //always true?
                    //difference is absorbed here
                    parent->balance = 0;
                    break;
                }
                assert(parent->balance == 0);
                if (current->balance == 0)
                {
                    parent->balance = 0;
                } else
                {
                    parent->balance = -1;
                }
            }
        }
        current = parent;
        parent = current->parent_;
    }

    return iterator(current);
}

BinaryNode* BinaryTree::plainInsert( std::unique_ptr<TreeData>&& data )
{
    if (!root_)
    {
        root_ = std::make_unique<BinaryNode>(nullptr, std::move(data));
        return root_.get();
    }

    auto current = root_.get();
    while(true)
    {
        assert( *current->data_ != *data); // key has to be unique
        if (*current->data_ > *data)
        {
            if (current->leftChild_)
            {
                current = current->leftChild_.get();
            } else
            {
                current->leftChild_ = std::make_unique<BinaryNode>(current, std::move(data));
                return current->leftChild_.get();
            }
        } else
        {
            if (current->rightChild_)
            {
                current = current->rightChild_.get();
            } else
            {
                current->rightChild_ = std::make_unique<BinaryNode>(current, std::move(data));
                return current->rightChild_.get();
            }
        }
    }
}

void BinaryTree::saveDOTFile( const std::string& filename )
{
    std::ofstream fout(filename);
    fout << "digraph BinaryTree {" << std::endl;

    if (root_)
    {
        newID = 1;
        saveDOTNode( fout, root_.get(), 0);
    }

    fout << "}" << std::endl;
    fout.close();
}

std::unique_ptr<BinaryNode>& BinaryTree::getUniquePtr(BinaryNode* node)
{
    if (node->parent_)
    {
        if (node->parent_->leftChild_.get() == node)
        {
            return node->parent_->leftChild_;
        } else
        {
            assert(node->parent_->rightChild_.get() == node);
            return node->parent_->rightChild_;
        }
    } else
    {
        assert(root_.get() == node);
        return root_;
    }
}

void BinaryTree::saveDOTNode( std::ofstream& fout, const BinaryNode* node, const int id)
{
    fout << id << "[label=\"" << node->balance << ": " << *(node->data_) << "\"];" << std::endl;
    if (node->leftChild_)
    {
        auto childID = newID;
        ++newID;
        fout << id << " -> " << childID << std::endl;
        saveDOTNode( fout, node->leftChild_.get(), childID);
    }
    if (node->rightChild_)
    {
        auto childID = newID;
        ++newID;
        fout << id << " -> " << childID << std::endl;
        saveDOTNode( fout, node->rightChild_.get(), childID);
    }
}
