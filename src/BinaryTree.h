#pragma once

#include "BinaryNode.h"
#include <cassert>
#include <memory>

class BinaryTree
{
public:
    class iterator : public std::iterator<std::bidirectional_iterator_tag, TreeData>
    {
    public:
        friend BinaryTree;
        friend bool operator==( const iterator& lhs, const iterator& rhs );
        friend bool operator!=( const iterator& lhs, const iterator& rhs );

        iterator() = default;
        iterator(const iterator& rhs) = default;
        iterator& operator=(const iterator& rhs) = default;

        iterator& operator++();
        iterator  operator++(int)
        {
            iterator tmp(*this); // copy
            operator++();        // pre-increment
            return tmp;          // return old value
        }

        TreeData& operator*() { return *(node_->data_); }
        iterator& operator+=(int inc) { for (auto i = 0; i<inc; ++i) { ++*this; } return *this; }
    private:
        iterator(BinaryNode* node) : node_(node) { }

        BinaryNode* node_ = nullptr;
    };

    BinaryTree();
    iterator begin() const;
    iterator end() const;
    iterator insert( std::unique_ptr<TreeData>&& data );

    void saveDOTFile( const std::string& filename );

    const std::unique_ptr<BinaryNode>& getRootNode() const {return root_;}

private:
    std::unique_ptr<BinaryNode>& getUniquePtr(BinaryNode* node);
    std::unique_ptr<BinaryNode>& rotateLeft( std::unique_ptr<BinaryNode>& parent, std::unique_ptr<BinaryNode>& rightChild );
    std::unique_ptr<BinaryNode>& rotateRight( std::unique_ptr<BinaryNode>& parent, std::unique_ptr<BinaryNode>& leftChild );
    std::unique_ptr<BinaryNode>& rotateRightLeft( std::unique_ptr<BinaryNode>& parent, std::unique_ptr<BinaryNode>& rightChild );
    std::unique_ptr<BinaryNode>& rotateLeftRight( std::unique_ptr<BinaryNode>& parent, std::unique_ptr<BinaryNode>& leftChild );
    BinaryNode* plainInsert( std::unique_ptr<TreeData>&& data );

    int newID = 0;
    void saveDOTNode( std::ofstream& fout, const BinaryNode* node, const int id);
    std::unique_ptr<BinaryNode> root_ = nullptr;
};


inline
bool operator==( const BinaryTree::iterator& lhs, const BinaryTree::iterator& rhs )
{
    return lhs.node_ == rhs.node_;
}
inline
bool operator!=( const BinaryTree::iterator& lhs, const BinaryTree::iterator& rhs )
{
    return !(lhs.node_ == rhs.node_);
}

inline
BinaryTree::iterator operator+( BinaryTree::iterator it, const int inc)
{
    it += inc;
    return it;
}
