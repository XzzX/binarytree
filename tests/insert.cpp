#include "gtest/gtest.h"

#include "BinaryTree.h"

#include <algorithm>
#include <vector>

struct c_unique {
    int current;
    c_unique() {current=0;}
    int operator()() {return ++current;}
};

TEST (InsertTest, ReturnValue)
{
    c_unique UniqueNumber;
    std::vector<int> values(100);
    std::generate(values.begin(), values.end(), UniqueNumber);
    unsigned seed = 1337;
    std::shuffle(values.begin(), values.end(), std::default_random_engine(seed));

    BinaryTree tree;
    std::for_each(values.begin(), values.end(), [&](int i){ ASSERT_EQ(*tree.insert(std::make_unique<TreeData>( i )), i);});
}

TEST (InsertTest, BalanceLabelLeftFirst)
{
    BinaryTree tree;

    tree.insert(std::make_unique<TreeData>( 10));
    {
        auto& treeIt = tree.getRootNode();
        EXPECT_EQ(  treeIt->balance    , 0 );
        EXPECT_EQ( *treeIt->data_      , 10 );
        EXPECT_EQ(  treeIt->leftChild_ , nullptr );
        EXPECT_EQ(  treeIt->rightChild_, nullptr );
    }

    tree.insert(std::make_unique<TreeData>( 5));
    {
        auto& treeIt = tree.getRootNode();
        EXPECT_EQ(  treeIt->balance    , -1 );
        EXPECT_EQ( *treeIt->data_      , 10 );
        EXPECT_NE(  treeIt->leftChild_ , nullptr );
        EXPECT_EQ(  treeIt->rightChild_, nullptr );

        EXPECT_EQ(  treeIt->leftChild_->balance    , 0 );
        EXPECT_EQ( *treeIt->leftChild_->data_      , 5 );
        EXPECT_EQ(  treeIt->leftChild_->leftChild_ , nullptr );
        EXPECT_EQ(  treeIt->leftChild_->rightChild_, nullptr );
    }

    tree.insert(std::make_unique<TreeData>( 15));
    {
        auto& treeIt = tree.getRootNode();
        EXPECT_EQ(  treeIt->balance    ,  0 );
        EXPECT_EQ( *treeIt->data_      , 10 );
        EXPECT_NE(  treeIt->leftChild_ , nullptr );
        EXPECT_NE(  treeIt->rightChild_, nullptr );

        EXPECT_EQ(  treeIt->leftChild_->balance    , 0 );
        EXPECT_EQ( *treeIt->leftChild_->data_      , 5 );
        EXPECT_EQ(  treeIt->leftChild_->leftChild_ , nullptr );
        EXPECT_EQ(  treeIt->leftChild_->rightChild_, nullptr );

        EXPECT_EQ(  treeIt->rightChild_->balance    , 0 );
        EXPECT_EQ( *treeIt->rightChild_->data_      , 15 );
        EXPECT_EQ(  treeIt->rightChild_->leftChild_ , nullptr );
        EXPECT_EQ(  treeIt->rightChild_->rightChild_, nullptr );
    }

}

TEST (InsertTest, BalanceLabelRightFirst)
{
    BinaryTree tree;

    tree.insert(std::make_unique<TreeData>( 10));
    {
        auto& treeIt = tree.getRootNode();
        EXPECT_EQ(  treeIt->balance    , 0 );
        EXPECT_EQ( *treeIt->data_      , 10 );
        EXPECT_EQ(  treeIt->leftChild_ , nullptr );
        EXPECT_EQ(  treeIt->rightChild_, nullptr );
    }

    tree.insert(std::make_unique<TreeData>( 15));
    {
        auto& treeIt = tree.getRootNode();
        EXPECT_EQ(  treeIt->balance    , 1 );
        EXPECT_EQ( *treeIt->data_      , 10 );
        EXPECT_EQ(  treeIt->leftChild_ , nullptr );
        EXPECT_NE(  treeIt->rightChild_, nullptr );

        EXPECT_EQ(  treeIt->rightChild_->balance    , 0 );
        EXPECT_EQ( *treeIt->rightChild_->data_      , 15 );
        EXPECT_EQ(  treeIt->rightChild_->leftChild_ , nullptr );
        EXPECT_EQ(  treeIt->rightChild_->rightChild_, nullptr );
    }

    tree.insert(std::make_unique<TreeData>( 5));
    {
        auto& treeIt = tree.getRootNode();
        EXPECT_EQ(  treeIt->balance    ,  0 );
        EXPECT_EQ( *treeIt->data_      , 10 );
        EXPECT_NE(  treeIt->leftChild_ , nullptr );
        EXPECT_NE(  treeIt->rightChild_, nullptr );

        EXPECT_EQ(  treeIt->rightChild_->balance    , 0 );
        EXPECT_EQ( *treeIt->rightChild_->data_      , 15 );
        EXPECT_EQ(  treeIt->rightChild_->leftChild_ , nullptr );
        EXPECT_EQ(  treeIt->rightChild_->rightChild_, nullptr );

        EXPECT_EQ(  treeIt->leftChild_->balance    , 0 );
        EXPECT_EQ( *treeIt->leftChild_->data_      , 5 );
        EXPECT_EQ(  treeIt->leftChild_->leftChild_ , nullptr );
        EXPECT_EQ(  treeIt->leftChild_->rightChild_, nullptr );
    }

}

TEST (InsertTest, rotateLeft)
{
    std::vector<int> values{10,15,20};
    BinaryTree tree;
    std::for_each(values.begin(), values.end(), [&](int i){tree.insert(std::make_unique<TreeData>( i ));});

    auto& treeIt = tree.getRootNode();
    EXPECT_EQ( treeIt->balance, 0 );
    EXPECT_EQ( *treeIt->data_ , 15 );

    EXPECT_EQ(  treeIt->leftChild_->balance    , 0 );
    EXPECT_EQ( *treeIt->leftChild_->data_      , 10 );
    EXPECT_EQ(  treeIt->leftChild_->rightChild_, nullptr );
    EXPECT_EQ(  treeIt->leftChild_->leftChild_ , nullptr );

    EXPECT_EQ(  treeIt->rightChild_->balance    , 0 );
    EXPECT_EQ( *treeIt->rightChild_->data_      , 20 );
    EXPECT_EQ(  treeIt->rightChild_->rightChild_, nullptr );
    EXPECT_EQ(  treeIt->rightChild_->leftChild_ , nullptr );
}

TEST (InsertTest, rotateRight)
{
    std::vector<int> values{10,5,2};
    BinaryTree tree;
    std::for_each(values.begin(), values.end(), [&](int i){tree.insert(std::make_unique<TreeData>( i ));});

    auto& treeIt = tree.getRootNode();
    EXPECT_EQ( treeIt->balance, 0 );
    EXPECT_EQ( *treeIt->data_ , 5 );

    EXPECT_EQ(  treeIt->leftChild_->balance    , 0 );
    EXPECT_EQ( *treeIt->leftChild_->data_      , 2 );
    EXPECT_EQ(  treeIt->leftChild_->rightChild_, nullptr );
    EXPECT_EQ(  treeIt->leftChild_->leftChild_ , nullptr );

    EXPECT_EQ(  treeIt->rightChild_->balance    , 0 );
    EXPECT_EQ( *treeIt->rightChild_->data_      , 10 );
    EXPECT_EQ(  treeIt->rightChild_->rightChild_, nullptr );
    EXPECT_EQ(  treeIt->rightChild_->leftChild_ , nullptr );
}

TEST (InsertTest, rotateRightLeft)
{
    std::vector<int> values{10,15,12};
    BinaryTree tree;
    std::for_each(values.begin(), values.end(), [&](int i){tree.insert(std::make_unique<TreeData>( i ));});

    auto& treeIt = tree.getRootNode();
    EXPECT_EQ( treeIt->balance, 0 );
    EXPECT_EQ( *treeIt->data_ , 12 );

    EXPECT_EQ(  treeIt->leftChild_->balance    , 0 );
    EXPECT_EQ( *treeIt->leftChild_->data_      , 10 );
    EXPECT_EQ(  treeIt->leftChild_->rightChild_, nullptr );
    EXPECT_EQ(  treeIt->leftChild_->leftChild_ , nullptr );

    EXPECT_EQ(  treeIt->rightChild_->balance    , 0 );
    EXPECT_EQ( *treeIt->rightChild_->data_      , 15 );
    EXPECT_EQ(  treeIt->rightChild_->rightChild_, nullptr );
    EXPECT_EQ(  treeIt->rightChild_->leftChild_ , nullptr );
}

TEST (InsertTest, rotateLeftRight)
{
    std::vector<int> values{10,5,7};
    BinaryTree tree;
    std::for_each(values.begin(), values.end(), [&](int i){tree.insert(std::make_unique<TreeData>( i ));});

    auto& treeIt = tree.getRootNode();
    EXPECT_EQ( treeIt->balance, 0 );
    EXPECT_EQ( *treeIt->data_ , 7 );

    EXPECT_EQ(  treeIt->leftChild_->balance    , 0 );
    EXPECT_EQ( *treeIt->leftChild_->data_      , 5 );
    EXPECT_EQ(  treeIt->leftChild_->rightChild_, nullptr );
    EXPECT_EQ(  treeIt->leftChild_->leftChild_ , nullptr );

    EXPECT_EQ(  treeIt->rightChild_->balance    , 0 );
    EXPECT_EQ( *treeIt->rightChild_->data_      , 10 );
    EXPECT_EQ(  treeIt->rightChild_->rightChild_, nullptr );
    EXPECT_EQ(  treeIt->rightChild_->leftChild_ , nullptr );
}

TEST (InsertTest, rotateLeftPropagate)
{
    std::vector<int> values{10, 5, 15, 20, 25};
    BinaryTree tree;
    std::for_each(values.begin(), values.end(), [&](int i){tree.insert(std::make_unique<TreeData>( i ));});

    auto& treeIt = tree.getRootNode();
    EXPECT_EQ( treeIt->balance, 0 );
    EXPECT_EQ( *treeIt->data_ , 10 );
    EXPECT_NE(  treeIt->rightChild_, nullptr );
    EXPECT_NE(  treeIt->leftChild_ , nullptr );

    EXPECT_EQ(  treeIt->leftChild_->balance    , 0 );
    EXPECT_EQ( *treeIt->leftChild_->data_      , 5 );
    EXPECT_EQ(  treeIt->leftChild_->rightChild_, nullptr );
    EXPECT_EQ(  treeIt->leftChild_->leftChild_ , nullptr );

    EXPECT_EQ(  treeIt->rightChild_->balance    , 0 );
    EXPECT_EQ( *treeIt->rightChild_->data_      , 20 );
    EXPECT_NE(  treeIt->rightChild_->rightChild_, nullptr );
    EXPECT_NE(  treeIt->rightChild_->leftChild_ , nullptr );

    EXPECT_EQ(  treeIt->rightChild_->rightChild_->balance    , 0 );
    EXPECT_EQ( *treeIt->rightChild_->rightChild_->data_      , 25 );
    EXPECT_EQ(  treeIt->rightChild_->rightChild_->rightChild_, nullptr );
    EXPECT_EQ(  treeIt->rightChild_->rightChild_->leftChild_ , nullptr );

    EXPECT_EQ(  treeIt->rightChild_->leftChild_->balance    , 0 );
    EXPECT_EQ( *treeIt->rightChild_->leftChild_->data_      , 15 );
    EXPECT_EQ(  treeIt->rightChild_->leftChild_->rightChild_, nullptr );
    EXPECT_EQ(  treeIt->rightChild_->leftChild_->leftChild_ , nullptr );
}

TEST (InsertTest, rotateRightPropagate)
{
    std::vector<int> values{10, 5, 15, 3, 2};
    BinaryTree tree;
    std::for_each(values.begin(), values.end(), [&](int i){tree.insert(std::make_unique<TreeData>( i ));});

    auto& treeIt = tree.getRootNode();
    EXPECT_EQ( treeIt->balance, 0 );
    EXPECT_EQ( *treeIt->data_ , 10 );
    EXPECT_NE(  treeIt->rightChild_, nullptr );
    EXPECT_NE(  treeIt->leftChild_ , nullptr );

    EXPECT_EQ(  treeIt->rightChild_->balance    , 0 );
    EXPECT_EQ( *treeIt->rightChild_->data_      , 15 );
    EXPECT_EQ(  treeIt->rightChild_->rightChild_, nullptr );
    EXPECT_EQ(  treeIt->rightChild_->leftChild_ , nullptr );

    EXPECT_EQ(  treeIt->leftChild_->balance    , 0 );
    EXPECT_EQ( *treeIt->leftChild_->data_      , 3 );
    EXPECT_NE(  treeIt->leftChild_->rightChild_, nullptr );
    EXPECT_NE(  treeIt->leftChild_->leftChild_ , nullptr );

    EXPECT_EQ(  treeIt->leftChild_->rightChild_->balance    , 0 );
    EXPECT_EQ( *treeIt->leftChild_->rightChild_->data_      , 5 );
    EXPECT_EQ(  treeIt->leftChild_->rightChild_->rightChild_, nullptr );
    EXPECT_EQ(  treeIt->leftChild_->rightChild_->leftChild_ , nullptr );

    EXPECT_EQ(  treeIt->leftChild_->leftChild_->balance    , 0 );
    EXPECT_EQ( *treeIt->leftChild_->leftChild_->data_      , 2 );
    EXPECT_EQ(  treeIt->leftChild_->leftChild_->rightChild_, nullptr );
    EXPECT_EQ(  treeIt->leftChild_->leftChild_->leftChild_ , nullptr );
}
