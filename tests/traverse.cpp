#include "gtest/gtest.h"

#include "BinaryTree.h"

#include <algorithm>
#include <random>
#include <vector>

struct c_unique {
    int current;
    c_unique() {current=0;}
    int operator()() {return ++current;}
} UniqueNumber;

TEST (TraverseTest, Simple)
{
    std::vector<int> values{10,5,15,3,7,13,17};
    BinaryTree tree;
    std::for_each(values.begin(), values.end(), [&](int i){tree.insert(std::make_unique<TreeData>( i ));});
    std::sort(values.begin(), values.end());
    auto valIt = values.begin();
    for (auto treeIt = tree.begin(); treeIt != tree.end(); ++treeIt)
    {
        ASSERT_NE(valIt, values.end()) << "no elements left in values";
        EXPECT_EQ(*treeIt, *valIt);
        ++valIt;
    }
    ASSERT_EQ(valIt, values.end()) << "end of values not reached\nwe are at: " << *valIt;
}

TEST (TraverseTest, LargeRandom)
{
    std::vector<int> values(100);
    std::generate(values.begin(), values.end(), UniqueNumber);
    unsigned seed = 1337;
    std::shuffle(values.begin(), values.end(), std::default_random_engine(seed));

    BinaryTree tree;
    std::for_each(values.begin(), values.end(), [&](int i){tree.insert(std::make_unique<TreeData>( i ));});
    std::sort(values.begin(), values.end());
    auto valIt = values.begin();
    for (auto treeIt = tree.begin(); treeIt != tree.end(); ++treeIt)
    {
        ASSERT_NE(valIt, values.end()) << "no elements left in values";
        EXPECT_EQ(*treeIt, *valIt);
        ++valIt;
    }
    ASSERT_EQ(valIt, values.end()) << "end of values not reached\nwe are at: " << *valIt;
}
