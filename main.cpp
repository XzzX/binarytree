#include "BinaryTree.h"

#include <algorithm>
#include <chrono>
#include <iostream>
#include <memory>
#include <random>
#include <vector>

// class generator:
struct c_unique {
    int current;
    c_unique() {current=0;}
    int operator()() {return ++current;}
} UniqueNumber;

int main()
{
    std::vector<int> values{10, 5, 15, 20, 25};
    //std::generate(values.begin(), values.end(), UniqueNumber);
    //unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    //std::shuffle(values.begin(), values.end(), std::default_random_engine(seed));

    BinaryTree tree;
        for (auto i : values)
        {
            std::cout << i << std::endl;
            tree.insert( std::make_unique<TreeData>( i ));
        }

//    tree.insert(std::make_unique<TreeData>(10));
//    tree.insert(std::make_unique<TreeData>(15));
//    tree.insert(std::make_unique<TreeData>(12));
//    tree.insert(std::make_unique<TreeData>( 5));
//    tree.saveDOTFile( "BinaryTree.dot" );
//    tree.insert(std::make_unique<TreeData>( 7));
//    tree.saveDOTFile( "BinaryTree.dot" );
//    tree.insert(std::make_unique<TreeData>( 3));
//    tree.insert(std::make_unique<TreeData>( 2));

    std::cout << "=============================" << std::endl;

    for (auto it = tree.begin(); it != tree.end(); ++it)
    {
        std::cout << *it << std::endl;
    }

    tree.saveDOTFile( "BinaryTree.dot" );

    return 0;
}
